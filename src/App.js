import { useRef, useCallback, useState } from "react";
import logo from "./logo.svg";
import Webcam from "react-webcam";
import axios from "axios";

const apiKey =
  "cdb29f355cb4059995e054208cd7c06a332acfb83a0a29592e88c58f78a7e4f8a8c0c3cfd71391e67b466dc00b475424ac";
const url = "https://nvision.nipa.cloud/api/v1/object-detection";
function App() {
  const handleFileRead = async (event) => {
    const file = event.target.files[0];
    const base64 = await convertBase64(file);
    setImageUpload(base64);
    setDetectedUpload([]);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  const webcamRef = useRef(null);
  const [image, setImage] = useState(null);
  const [imageUpload, setImageUpload] = useState(null);
  const [detected, setDetected] = useState([]);
  const [detectedUpload, setDetectedUpload] = useState([]);
  const onCapture = useCallback(() => {
    const imageSrc = webcamRef.current.getScreenshot();
    setImage(imageSrc);
    var temp = imageSrc.split("base64,");
    axios
      .post(
        url,
        {
          raw_data: temp[1],
        },
        {
          headers: {
            Authorization: `ApiKey ${apiKey}`,
          },
        }
      )
      .then((res) => {
        if (res.status === 200) {
          setDetected(res.data.detected_objects);
        }
      });
  }, [webcamRef]);

  const onUpload = () => {
    if (imageUpload) {
      var temp = imageUpload.split("base64,");
      axios
        .post(
          url,
          {
            raw_data: temp[1],
          },
          {
            headers: {
              Authorization: `ApiKey ${apiKey}`,
            },
          }
        )
        .then((res) => {
          if (res.status === 200) {
            setDetectedUpload(res.data.detected_objects);
          }
        });
    } else {
      alert("Please choose file");
    }
  };
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-10">
          <div className="card">
            <div className="card-body">
              <div className="text-center">
                <img src={logo} className="App-logo" alt="logo" />
                <h2>Nvision Object Detection</h2>
              </div>
              <hr />
              <p className="text-center">
                1. กดปุ่ม Capture ด้านซ้ายมือเพื่อแสดงผลการ detected รูปภาพ
              </p>
              <div className="row">
                <div className="col-6">
                  <Webcam
                    ref={webcamRef}
                    width={"100%"}
                    screenshotFormat="image/jpeg"
                  />
                  <div className="text-center">
                    <button className="btn btn-primary" onClick={onCapture}>
                      Capture
                    </button>
                  </div>
                </div>
                <div className="col-6">
                  {detected.length !== 0 && (
                    <div className="wrapper-img">
                      {detected.map((item, index) => (
                        <div
                          className="bounding-box"
                          key={index}
                          style={{
                            top: `${item.bounding_box.top}px`,
                            left: `${item.bounding_box.left}px`,
                            right: `${509 - item.bounding_box.right}px`,
                            bottom: `${381 - item.bounding_box.bottom}px`,
                          }}
                        >
                          <span>
                            {"[" +
                              item.parent +
                              "] " +
                              item.name +
                              " : " +
                              item.confidence.toFixed(2)}
                          </span>
                        </div>
                      ))}

                      <img src={image} alt="test" />
                    </div>
                  )}
                </div>
              </div>
              <hr />
              <p className="text-center">
                2. กดปุ่ม Upload รูปภาพเพื่อแสดงผลการ detected รูปภาพ
              </p>
              <div className="row">
                <div className="col-6">
                  <div className="input-group">
                    <button
                      className="btn btn-outline-secondary"
                      onClick={onUpload}
                    >
                      Upload
                    </button>
                    <input
                      type="file"
                      className="form-control"
                      accept=".jpef, .png, .jpg"
                      onChange={(e) => handleFileRead(e)}
                    />
                  </div>
                  {imageUpload && (
                    <img src={imageUpload} alt="test" className="img-fluid" />
                  )}
                </div>
                <div className="col-6">
                  {detectedUpload.length !== 0 && (
                    <ul className="list-group">
                      <li className="list-group-item active">รายการ Object</li>
                      {detectedUpload.map((item, index) => (
                        <li className="list-group-item" key={index}>
                          <span>
                            {"[" +
                              item.parent +
                              "] " +
                              item.name +
                              " : " +
                              item.confidence.toFixed(2)}
                          </span>
                        </li>
                      ))}
                    </ul>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
